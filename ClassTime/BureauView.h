//
//  BureauView.h
//  BureauViewTest
//
//  Created by enfant on 14-09-07.
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import <CoreGraphics/CoreGraphics.h>
#import <QuartzCore/QuartzCore.h>

@interface BureauView : UIControl <UITableViewDataSource, UITableViewDelegate> {
    int placeSelectionee;
    NSMutableArray *_places;
    NSMutableArray *_nom;
    NSMutableArray *nomComplet;
    NSMutableArray *_visible;
    BOOL changementEleve;
    UITableView *_tableView;
    int _place;
}
@property BOOL deplaceable;
@property BOOL interchangeable;

-(int)placeForPoint:(CGPoint)point;
-(void)newPlace:(CGPoint)point;
-(void)newBureau:(CGPoint)point withName:(NSString *)nom;
-(CGPoint)placeForStudent:(NSString *)eleve;
-(NSArray *)students;

@end
