//
//  BureauView.m
//  BureauViewTest
//
//  Created by enfant on 14-09-07.
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "BureauView.h"

@implementation BureauView

@synthesize deplaceable;
@synthesize interchangeable = changementEleve;

#pragma mark -
#pragma mark init

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self.superview bringSubviewToFront:self];
        deplaceable = 0;
        _places = [[NSMutableArray alloc] init];
        _nom = [[NSMutableArray alloc] init];
        _visible = [[NSMutableArray alloc] init];
        nomComplet = [[NSMutableArray alloc]init];
        changementEleve = TRUE;
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(self.frame.size.width*0.8-4.0,10.0,self.frame.size.width*0.2,self.frame.size.height-20.0) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [self addSubview:_tableView];
        [self setNeedsDisplay];
        _place = -1;
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]){
        [self.superview bringSubviewToFront:self];
        deplaceable = 0;
        _places = [[NSMutableArray alloc] init];
        _nom = [[NSMutableArray alloc] init];
        _visible = [[NSMutableArray alloc] init];
        nomComplet = [[NSMutableArray alloc]init];
        changementEleve = TRUE;
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(self.frame.size.width*0.8-4.0,10.0,self.frame.size.width*0.2,self.frame.size.height-20.0) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [self addSubview:_tableView];
        [self setNeedsDisplay];
        _place = -1;
    }
    return self;
}

#pragma mark -
#pragma mark viewDidUnload

-(void)viewDidUnload{
}


#pragma mark -
#pragma mark drawRect:

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    //Drawing code
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    [[UIColor whiteColor] setFill];
    [[UIColor blackColor] setStroke];
    CGContextFillRect(ctx, rect);
    UIBezierPath *path = [UIBezierPath bezierPathWithRect:self.bounds];
    path.lineWidth = 4.0;
    [path stroke];
    path = [UIBezierPath bezierPathWithRect:CGRectMake(self.frame.size.width*0.8-4.0,10.0,self.frame.size.width*0.2,self.frame.size.height-20.0)];
    path.lineWidth = 4.0;
    [path stroke];
    path = nil;
    for (NSValue *_point in _places) {
        CGPoint place = [_point CGPointValue];
        if ((place.x > 41.0) && (place.x < self.frame.size.width-41.0) && (place.y > 21.0) && (place.y < self.frame.size.width-21.0)) {
            CGRect bureau = CGRectMake(place.x-40, place.y-30, 80, 60);
            UIBezierPath *bureauPath = [UIBezierPath bezierPathWithRoundedRect:bureau cornerRadius:10.0];
            bureauPath.lineWidth = 4.0;
            if (placeSelectionee == [_places indexOfObject:_point]){
                [[UIColor grayColor] setFill];
            } else {
                [[UIColor whiteColor] setFill];
            }
            [bureauPath fill];
            [bureauPath stroke];   
            bureauPath = nil;
            UILabel *nom = [_nom objectAtIndex:[_places indexOfObject:_point]];
            nom.hidden = ![[_visible objectAtIndex:[_places indexOfObject:_point]]boolValue]; 
            nom.frame = CGRectInset(bureau, 4.0, 4.0);
        }
    }
    self.layer.masksToBounds = NO;
    self.layer.shadowOffset = CGSizeMake(-15, 20);
    self.layer.shadowRadius = 5;
    self.layer.shadowOpacity = 0.5;
}


#pragma mark -
#pragma mark tracking

-(BOOL)beginTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event{
    [super beginTrackingWithTouch:touch withEvent:event];
    placeSelectionee = [self placeForPoint:[touch locationInView:self]];
    if(CGRectContainsPoint(CGRectMake(self.frame.size.width*0.8-4.0,10.0,self.frame.size.width*0.2,self.frame.size.height-20.0), [touch locationInView:self]) && !changementEleve){
        changementEleve = true;
        [self setNeedsDisplay];
    } else if (!CGRectContainsPoint(CGRectMake(self.frame.size.width*0.8-4.0,10.0,self.frame.size.width*0.2,self.frame.size.height-20.0), [touch locationInView:self]) && changementEleve){
        [_tableView beginUpdates];
        [_tableView deselectRowAtIndexPath:[_tableView indexPathForSelectedRow] animated:YES];
        [_tableView endUpdates];
        changementEleve = false;
        [self setNeedsDisplay];
    }
    if (_place != -1 && placeSelectionee != -1){
        CGPoint previousPoint = [[_places objectAtIndex:placeSelectionee] CGPointValue];
        [_places replaceObjectAtIndex:placeSelectionee withObject:[_places objectAtIndex:_place]];
        [_places replaceObjectAtIndex:_place withObject:[NSValue valueWithCGPoint:previousPoint]];
        [_visible replaceObjectAtIndex:(NSUInteger)_place withObject:[NSNumber numberWithBool:YES]];
        [self setNeedsDisplay];
        _place = -1;
        [self sendActionsForControlEvents:UIControlEventValueChanged];
        [_tableView beginUpdates];
        [_tableView deselectRowAtIndexPath:[_tableView indexPathForSelectedRow] animated:YES];
        [_tableView endUpdates];
        return false;
    }
    if ((!deplaceable) || (changementEleve) || placeSelectionee == -1){
        return false;
    }
    return true;
}

-(BOOL)continueTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event{
    [super continueTrackingWithTouch:touch withEvent:event];
    if([touch locationInView:self].x > self.bounds.size.width-_tableView.bounds.size.width-36.0 || [touch locationInView:self].x < 44.0 || [touch locationInView:self].y > self.bounds.size.height-26.0 || [touch locationInView:self].y < 34.0) return TRUE;
    [_places replaceObjectAtIndex:(NSUInteger)placeSelectionee withObject:[NSValue valueWithCGPoint:[touch locationInView:self]]];
    [self setNeedsDisplay];
    [self sendActionsForControlEvents:UIControlEventValueChanged];
    return true;
}

-(void)endTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event{
    [super endTrackingWithTouch:touch withEvent:event];
    if([touch locationInView:self].x > self.bounds.size.width*0.9-40.0 || [touch locationInView:self].x < 40.0 || [touch locationInView:self].y > self.bounds.size.height-30.0 || [touch locationInView:self].y < 30.0) return;
    [_places replaceObjectAtIndex:(NSUInteger)placeSelectionee withObject:[NSValue valueWithCGPoint:[touch locationInView:self]]];
    placeSelectionee = -1;
    [self setNeedsDisplay];
    [self sendActionsForControlEvents:UIControlEventValueChanged];
}

#pragma mark -
#pragma mark placeForPoint

-(int)placeForPoint:(CGPoint)point{
    for (NSValue *__point in _places) {
        CGPoint _point = [__point CGPointValue];
        if((point.x < (_point.x + 40)) && (point.x > (_point.x -40)) && (point.y < (_point.y + 30)) && (point.y > (_point.y - 30))){
            return (int)[_places indexOfObject:[NSValue valueWithCGPoint:_point]];
        }
    }
    return -1;
}

#pragma mark -
#pragma mark nouveau bureau

-(void)newPlace:(CGPoint)point{
    [_places addObject:[NSValue valueWithCGPoint:point]];
    CGRect bureau = CGRectMake(point.x-40, point.y-20, 80, 40);
    UILabel *nom = [[UILabel alloc] initWithFrame:CGRectInset(bureau, 4.0, 4.0)];
    nom.font = [UIFont fontWithName:@"Herculanum" size:16.0];
    nom.textAlignment = NSTextAlignmentCenter;
    nom.text = [NSString stringWithFormat:@"%lu",[_nom count]+1];
    nom.userInteractionEnabled = FALSE;
    [nomComplet addObject:[NSString stringWithFormat:@"%lu",[_nom count]+1]];
    [self addSubview:nom];
    [_nom addObject:nom];
    [_visible addObject:[NSNumber numberWithBool:YES]];
    [_tableView reloadData];
    [self setNeedsDisplay];
}

-(void)newBureau:(CGPoint)point withName:(NSString *)__nom{
    [_places addObject:[NSValue valueWithCGPoint:point]];
    CGRect bureau = CGRectMake(point.x-40, point.y-20, 80, 40);
    UILabel *nom = [[UILabel alloc] initWithFrame:CGRectInset(bureau, 4.0, 4.0)];
    nom.font = [UIFont fontWithName:@"Herculanum" size:16.0];
    nom.textAlignment = NSTextAlignmentCenter;
    nom.text = [[__nom componentsSeparatedByString:@" "] firstObject];
    nom.userInteractionEnabled = FALSE;
    [nomComplet addObject:__nom];
    [self addSubview:nom];
    [_nom addObject:nom];
    [_visible addObject:[NSNumber numberWithBool:YES]];
    [_tableView reloadData];
    [self setNeedsDisplay];
}


#pragma mark -
#pragma mark tableView delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_nom count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
    }
    
    // Configure the cell...
    cell.textLabel.text = [NSString stringWithFormat:@"%@: %@",((UILabel *)[_nom objectAtIndex:indexPath.row]).text,[nomComplet objectAtIndex:indexPath.row]];
    cell.textLabel.font = [UIFont fontWithName:@"Herculanum" size:16.0];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    cell.textLabel.numberOfLines = 0;
    
    return cell;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return (_tableView.bounds.size.height/[_nom count]);
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //if (!changementEleve) return; 
    if ([[_visible objectAtIndex:indexPath.row]boolValue]){
        [_visible replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithBool:![[_visible objectAtIndex:indexPath.row]boolValue]]];
        [self setNeedsDisplay];
    }
    _place = (int)indexPath.row;
}

#pragma mark -
#pragma mark placeForStudent:

-(CGPoint)placeForStudent:(NSString *)eleve{
    if ([nomComplet indexOfObject:eleve] != -1) {
        return [[_places objectAtIndex:[nomComplet indexOfObject:eleve]] CGPointValue];
    }
    NSLog(@"can't return real, %@, %lu",eleve,[nomComplet indexOfObject:eleve]);
    return CGPointMake(0, 0);
}

-(NSArray*)students{
    return [nomComplet copy];
}

@end
