//
//  Classe.h
//  ClassTime
//
//  Created by expo Science on 15-01-26.
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Classes, Eleve;

@interface Classe : NSManagedObject

@property (nonatomic) int16_t numero_de_classe;
@property (nonatomic, retain) NSString * devoirs;
@property (nonatomic, retain) Classes *classes;
@property (nonatomic, retain) NSSet *eleves;
@end

@interface Classe (CoreDataGeneratedAccessors)

- (void)addElevesObject:(Eleve *)value;
- (void)removeElevesObject:(Eleve *)value;
- (void)addEleves:(NSSet *)values;
- (void)removeEleves:(NSSet *)values;
@end
