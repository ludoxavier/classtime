//
//  Classe.m
//  ClassTime
//
//  Created by expo Science on 15-01-26.
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "Classe.h"
#import "Classes.h"
#import "Eleve.h"


@implementation Classe

@dynamic numero_de_classe;
@dynamic devoirs;
@dynamic classes;
@dynamic eleves;

@end
