//
//  Classes.h
//  ClassTime
//
//  Created by expo Science on 15-01-26.
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Classe;

@interface Classes : NSManagedObject

@property (nonatomic, retain) NSString * adresseCourriel;
@property (nonatomic, retain) NSSet *classes;
@end

@interface Classes (CoreDataGeneratedAccessors)

- (void)addClassesObject:(Classe *)value;
- (void)removeClassesObject:(Classe *)value;
- (void)addClasses:(NSSet *)values;
- (void)removeClasses:(NSSet *)values;
@end
