//
//  Eleve.h
//  ClassTime
//
//  Created by expo Science on 15-01-24.
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Classe;

@interface Eleve : NSManagedObject

@property (nonatomic) int32_t numero_d_eleve;
@property (nonatomic, retain) NSString * compte_rendu;
@property (nonatomic, retain) NSString * nom;
@property (nonatomic, retain) id place;
@property (nonatomic, retain) Classe *classe;

@end
