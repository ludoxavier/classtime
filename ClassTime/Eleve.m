//
//  Eleve.m
//  ClassTime
//
//  Created by expo Science on 15-01-24.
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "Eleve.h"
#import "Classe.h"


@implementation Eleve

@dynamic numero_d_eleve;
@dynamic compte_rendu;
@dynamic nom;
@dynamic place;
@dynamic classe;

@end
