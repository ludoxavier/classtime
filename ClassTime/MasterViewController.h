//
//  MasterViewController.h
//  ClassTime
//
//  Created by expo Science on 15-01-24.
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <CoreData/CoreData.h>

@class Classe, Classes, Eleve,ClasseViewController;

@interface MasterViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    NSArray *classe;
}

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong,nonatomic) Classes* classes;
@property (strong,nonatomic) IBOutlet UITableView *tableView;
@property (strong,nonatomic) NSData *CSVData;

-(void)reloadClasses;

@end
