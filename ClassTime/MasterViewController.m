//
//  MasterViewController.m
//  ClassTime
//
//  Created by expo Science on 15-01-24.
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "MasterViewController.h"

#import "Classe.h"
#import "Classes.h"
#import "Eleve.h"
#import "createClassViewContrller.h"
#import "ClassViewController.h"

@interface MasterViewController ()
@end

@implementation MasterViewController

@synthesize fetchedResultsController = __fetchedResultsController;
@synthesize managedObjectContext = __managedObjectContext;
@synthesize classes;
@synthesize tableView = _tableView;
@synthesize CSVData;

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    // Set up the edit and add buttons.
    //NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"description" ascending:YES];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Classes" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    classes = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] lastObject];
    fetchRequest = nil;
    entity = nil;
    classe = [classes.classes allObjects];
    [_tableView reloadData];
    if (CSVData != nil) [self performSegueWithIdentifier:@"newClass" sender:self];
    self.edgesForExtendedLayout = UIRectEdgeNone;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    _tableView = nil;
    for (UIView *view in [self.view subviews]) {
        [view removeFromSuperview];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Classes" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    classes = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] lastObject];
    fetchRequest = nil;
    entity = nil;
    classe = [classes.classes allObjects];
    [_tableView reloadData];
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

#pragma mark -
#pragma mark tableView data source & delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [classes.classes count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
        
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        
    }
    
    Classe *item = (Classe *)[classe objectAtIndex:indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"%d",item.numero_de_classe];
    if ([cell.textLabel.text isEqualToString:@"0"]) cell.textLabel.text = @"base";
    cell.textLabel.font = [UIFont boldSystemFontOfSize:24.0];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%lu  eleves",(unsigned long)[item.eleves count]];
    cell.detailTextLabel.textColor = [UIColor colorWithWhite:0.1 alpha:90.0];
    cell.detailTextLabel.font = [UIFont systemFontOfSize:18.0];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    
    
    return cell;
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
}

#pragma mark -
#pragma mark Table view selection

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    /*
     When a row is selected, the segue creates the detail view controller as the destination.
     Set the detail view controller's detail item to the item associated with the selected row.
     */
    if ([[segue identifier] isEqualToString:@"displayClass"]) {
        NSIndexPath *selectedRowIndex = [self.tableView indexPathForSelectedRow];
        ClassViewController *classView = (ClassViewController *)[segue destinationViewController];
        classView.classe = [classe objectAtIndex:selectedRowIndex.row];
        classView.classes = self.classes;
        classView.context = self.managedObjectContext;
        classView = nil;
        
    } else if([[segue identifier] isEqualToString:@"newClass"]) {
       createClassViewContrller *newClass = [segue destinationViewController];
        newClass.context = self.managedObjectContext;
        newClass.classes = self.classes;
        [newClass initDataWithCSV:CSVData];
        newClass = nil;
    }
}

-(void)reloadClasses{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Classes" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    classes = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] lastObject];
    fetchRequest = nil;
    entity = nil;
    classe = [classes.classes allObjects];
    [_tableView reloadData];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [classes removeClassesObject:[classe objectAtIndex:indexPath.row]];
    classe = [classes.classes allObjects];
    [self.managedObjectContext save:nil];
    [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    [_tableView reloadData];
}

-(void)dealloc{
    CSVData = nil;
    __managedObjectContext = nil;
    __fetchedResultsController = nil;
    classes = nil;
    classe = nil;
}

@end
