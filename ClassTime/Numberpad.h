#import <UIKit/UIKit.h>

@interface Numberpad : UIViewController

+ (Numberpad *)defaultNumberpad;
- (void)replaceTextOfTextInput:(UITextField*)textInput inTextRange:(UITextRange *)   textRange  withString:(NSString *)      string;

@end
