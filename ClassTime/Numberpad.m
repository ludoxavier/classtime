#import "Numberpad.h"

#pragma mark - Private methods

@interface Numberpad ()

@property (nonatomic, weak) UITextField *target;

@end

#pragma mark - Numberpad Implementation

@implementation Numberpad

@synthesize target;

#pragma mark - Singleton method

+ (Numberpad *)defaultNumberpad
{
    static Numberpad *defaultNumberpad = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        defaultNumberpad = [[Numberpad alloc] init];
    });
    return defaultNumberpad;
}

#pragma mark - view lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(editingDidBegin:) 
                                                 name:UITextFieldTextDidBeginEditingNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(editingDidEnd:) 
                                                 name:UITextFieldTextDidEndEditingNotification
                                               object:nil];
}

- (void)viewDidUnload
{
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:UITextFieldTextDidBeginEditingNotification 
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:UITextFieldTextDidEndEditingNotification 
                                                  object:nil];
    self.target = nil;
    
    [super viewDidUnload];
}

#pragma mark - editingDidBegin/End

- (void)editingDidBegin:(NSNotification *)notification {
    if (![notification.object conformsToProtocol:@protocol(UITextInput)]) {
        self.target = nil;
        return;
    } 
    
    self.target = notification.object;
}

- (void)editingDidEnd:(NSNotification *)notification {
    self.target = nil;
}

#pragma mark - Keypad IBAction's

- (IBAction)numberpadNumberPressed:(UIButton *)sender {
    if (self.target == nil) {
        return;
    }
    
    NSString *numberPressed  = sender.titleLabel.text;
    if ([numberPressed length] == 0) {
        return;
    }
    
    UITextRange *selectedTextRange = self.target.selectedTextRange;
    if (selectedTextRange == nil) {
        return;
    }
    
    [self replaceTextOfTextInput:self.target inTextRange:selectedTextRange withString:numberPressed];
}
- (IBAction)baseClass:(id)sender {
    if (self.target == nil) {
        return;
    }
    
    self.target.text = @"classe de base";
}

- (IBAction)numberpadDeletePressed:(UIButton *)sender {
    if (self.target == nil) {
        return;
    }
    
    UITextRange *selectedTextRange = self.target.selectedTextRange;
    if (selectedTextRange == nil) {
        return;
    }
    
    UITextPosition  *startPosition  = [self.target positionFromPosition:selectedTextRange.start offset:-1];
    if (startPosition == nil) {
        return;
    }
    UITextPosition  *endPosition    = selectedTextRange.end;
    if (endPosition == nil) {
        return;
    }
    UITextRange     *rangeToDelete  = [self.target textRangeFromPosition:startPosition
                                                              toPosition:endPosition];
    
    [self replaceTextOfTextInput:self.target inTextRange:rangeToDelete withString:@""];
}

- (IBAction)numberpadClearPressed:(UIButton *)sender {
    if (self.target == nil) {
        return;
    }
    
    UITextRange *allTextRange = [self.target textRangeFromPosition:self.target.beginningOfDocument
                                                        toPosition:self.target.endOfDocument];
    
    [self replaceTextOfTextInput:self.target inTextRange:allTextRange withString:@""];
}

// The done button was just pressed on the number pad
- (IBAction)numberpadDonePressed:(UIButton *)sender {
    if (self.target == nil) {
        return;
    }

    // Call the delegate methods and resign the first responder if appropriate
    if ([self.target isKindOfClass:[UITextField class]]) {
        UITextField *textField = (UITextField *)self.target;
        if ([textField.delegate respondsToSelector:@selector(textFieldShouldEndEditing:)]) {
            if ([textField.delegate textFieldShouldEndEditing:textField])
            {
                [textField resignFirstResponder];
            }
        }
    }
    
}

#pragma mark - replaceText

- (void)replaceTextOfTextInput:(UITextField *)textInput
                   inTextRange:(UITextRange *)   textRange 
                    withString:(NSString *)      string {
    if (textInput == nil) {
        return;
    }
    if (textRange == nil) {
        return;
    }
    
    int startPos                    = (int)[textInput offsetFromPosition:textInput.beginningOfDocument toPosition:textRange.start];
    int length                      = (int)[textInput offsetFromPosition:textRange.start
                                                         toPosition:textRange.end];
    NSRange selectedRange           = NSMakeRange(startPos, length);
    
    if ([textInput isKindOfClass:[UITextField class]]) 
    {
        if ([textInput.delegate respondsToSelector:@selector(textField:shouldChangeCharactersInRange:replacementString:)])
        {
            if (![textInput.delegate textField:textInput
                 shouldChangeCharactersInRange:selectedRange
                             replacementString:string]) 
            {
                return;
            }
        }
    }
    
    [textInput replaceRange:textRange withText:string];
}

@end
